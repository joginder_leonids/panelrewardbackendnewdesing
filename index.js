const express = require('express');
// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
var fileUpload = require('express-fileupload');



// create express app
const app = express();
const crypto = require('crypto');
const cookie = require('cookie');
const bodyParser = require('body-parser');
const querystring = require('querystring');
const adminUser = require('./admin/Routes');
const registerRouter = require('./registration/Routes');
const loginRouter = require('./logIn/Routes');
const profileRouter = require('./profile/Routes');
const studyRouter = require('./study/Routes');
const participateRouter = require('./studyParticipate/Routes');
const profileDashbordRouter = require('./profileDashbord/Routes');
const dashboard = require('./dashboard/Routes');
const redeemption = require('./redeemption/Routes');
const coupon = require('./giftCoupon/Routes');
const userImageUploadRouter = require('./public/usersUploadImage');
const userDisabledRouter = require('./userDisable/Routes');
const bluckDataApi  = require('./restApi/Routes');

const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = 'read_products';
const forwardingAddress = "node"; // Replace this with your HTTPS Forwarding address

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// csv file upload 
app.use(fileUpload());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// -------- Enable Cors --------//   
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/api', adminUser);
app.use('/api', registerRouter);
app.use('/api', loginRouter);
app.use('/api', profileRouter);
app.use('/api', studyRouter);
app.use('/api', participateRouter);
app.use('/api', profileDashbordRouter);
app.use('/api', dashboard);
app.use('/api', redeemption);
app.use('/api', coupon);
app.use('/api', userImageUploadRouter);
app.use('/api', bluckDataApi);
app.use('/api', userDisabledRouter);

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect("mongodb://localhost:27017/gnn-participation")
.then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

 app.use(express.static(__dirname,''));


app.get('/', (req, res) => {
  res.send('Hello World!');
});
//let port = process.env.PORT || 8810;
let port = process.env.PORT || 8811;
app.listen(port, () => {
  console.log('Example app listening on port 8811!');
});
