var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

var giftCouponModel = new Schema({
    Coupon_code: { type: String },
    Coupon_type:  {type: String},
    Description  : {type: String},
    Expire_date  : {type: Date },
    Coupon_status  : {type: String},
    Coupon_amount : {type: String},
    Is_Active  : {type : Boolean},
    createdAt:  {type: Date },
    updatedAt:  {type: Date}
});


var assignedGiftcouponModel = new Schema({
   
    user_id   : {type : String},
    transactionHistory_id : { type: Schema.Types.ObjectId , ref: 'transactionhistories' },
    couponId :  { type : Array , default : [] },
    totalAmount : {type : String},
    totalPoint : { type: String},
    isActive : {type : Boolean},
    createdAt: {type: Date},
    updatedAt: {type: Date}
})


giftCoupon = mongoose.model('gift_coupons',giftCouponModel);
assignedGiftcoupon = mongoose.model('assigns_giftcoupon', assignedGiftcouponModel);

module.exports = {

    giftCoupon  : giftCoupon,
    assignedGiftcoupon : assignedGiftcoupon    
};