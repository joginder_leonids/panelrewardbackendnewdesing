var  mongoose = require('mongoose'),
 csv = require('fast-csv'),
 coupon = require('./Model'),
 admin = require('../admin/Model'),
 hash = require('../common/cryptohash');
 TransactionHistory = require('../redeemption/Model');


function addCoupons(req, res){
    // console.log(req.files);
    let check =0;
    if (!req.files)
		return res.status(400).send('No files were uploaded.');
	
	var authorFile = req.files.file;

	var coupons = [];
		
	csv
	 .fromString(authorFile.data.toString(), {
		 headers: true,
		 ignoreEmpty: true
	 })
	 .on("data", function(data){
        check= check+1;
        if(data.Coupon_code && data.Coupon_type && data.Description  && data.Expire_date && data.Coupon_amount){
            var expireDate = new Date(data.Expire_date);
            
            data['_id'] = new mongoose.Types.ObjectId();
            data['Coupon_status'] = "active"
            data['Is_Active'] = "0";
            data['Expire_date'] = expireDate;
            data['createdAt'] = new Date();
            data['updatedAt'] = new Date();
            coupons.push(data);
        }
	 })
	 .on("end", function(){
         if(coupons.length>0){
            coupon.giftCoupon.create(coupons, function(err, documents) {
                if (err){
                    res.status(400).send({success:false, error :err});
                }else{
                res.status(200).send({success: true, message: 'Coupons have been successfully uploaded.'});
                }
             });
         }else{
            res.status(400).send({success:false, error :'check column in csv file'});
         }
	 });
}

function allCouponByAdmin(req, res){

    admin.find({Token : req.params.token},function(err, success){
        if(success.length>0){
            var d = new Date();
            d.setDate(d.getDate()-1);
            let newDate = d.toISOString();

            coupon.giftCoupon.find({"Coupon_status" : "active", Expire_date: { $gte: newDate}}).exec(function(err, data){
                if(err){
                    res.status(400).send({success:false, error :err});
                }else if(data && data.length>0){
                    res.status(200).send({success: true, couponList: data});
                }else{
                    res.status(400).send({success: false, error : "You haven't any coupons "});
                }
            });
    }else{
            res.status(200).send({ success: false, message : "admin is not valid "});
        }
    }); 
}


function getAllCouponCountByAdmin(req, res){

    let couponCountArray ={};

    admin.find({Token : req.params.token},function(err, success){
        if(success.length>0){
            var d = new Date();
            d.setDate(d.getDate()-1);
            let newDate = d.toISOString();

            coupon.giftCoupon.find({"Coupon_status" : "active", Expire_date: { $gte: newDate}}).exec(function(err, data){
                if(err){
                    res.status(400).send({success:false, error :err});
                }else if(data && data.length>0){
                    
                    // coupon.giftCoupon.aggregate(
                    //     { $group : { _id : { "Coupon_amount" : "$Coupon_amount", "_id" : "$_id" }}},
                    //     { $group : { _id : "$_id.Coupon_amount", count : { $sum : 1 } } }
                    //     ).exec(function(err, data){
                    //         if(err){
                    //             res.status(400).send({success:false, error :err});
                    //         }
                    //     console.log(data)
                    // });
                    // coupon.giftCoupon.distinct("Coupon_amount",{"Coupon_status" : "active"}).exec(function(err, uniqueCouponAmount){
                    //     if(err){
                    //         res.status(400).send({success:false, error :err});
                    //     }
                    //     if(uniqueCouponAmount.length>0){
                            
                         //  for(var unique= 0; unique < uniqueCouponAmount.length; unique++){
                               let count500 = 0;
                               let count100 = 0;
                               let count50 = 0;
                               let count20 = 0;
                               let count10 = 0;
                                for(var copno = 0; copno< data.length; copno++ ){

                                    if( "500"=== data[copno].Coupon_amount){
                                        count500 = count500 + 1;
                                    }else if("100"=== data[copno].Coupon_amount){
                                        count100 = count500 + 1;
                                    }else if("50"=== data[copno].Coupon_amount){
                                        count50 = count50 + 1;
                                    }else if("20"=== data[copno].Coupon_amount){
                                        count20 = count20 + 1;
                                    }else if("10"=== data[copno].Coupon_amount){
                                        count10 = count10 + 1;
                                    }
                                }
                                couponCountArray.C500 = count500;
                                couponCountArray.C100 = count100;
                                couponCountArray.C50 = count50;
                                couponCountArray.C20 = count20;
                                couponCountArray.C10 = count10;
                                
                                
                           //}
                           res.status(200).send({success: true, countData: couponCountArray});
                    //     }
                    // });

                }else{
                    res.status(400).send({success: false, error : "You haven't any coupons "});
                }
            });
    }else{
            res.status(200).send({ success: false, message : "admin is not valid "});
        }
    }); 
}


function assignGiftcouponOnline(req, res){

    let assignedCouponId = "";
    let arrayOfCouponsId = [];
    let arrayCoupon = req.body.arrayCoupon;
    let userId = req.body.userID;
    let transactionID = req.body.transactionID;
    let totalPoints = req.body.totalPoints;
    let totalAmount = req.body.totalAmount;
    
    
    //getALLTransactionID
    getALLTransactionID(arrayCoupon ,function(couponIdData){
        if(couponIdData.length>0){
            arrayOfCouponsId.push(couponIdData);
           //update all coupon status
           updateALLTransactionID(couponIdData ,function(result){
            if(result){
               
                //insert into assignedGiftcouponModel
                let assignedGiftcouponData = new coupon.assignedGiftcoupon({
                    user_id   :  userId,
                    transactionHistory_id     :  transactionID,
                    couponId         :  arrayOfCouponsId[0],
                    totalAmount    :  totalAmount,
                    totalPoint :  totalPoints,
                    isActive :  true,
                    created_at   :  new Date(),
                    updated_at   :  new Date()
                });
    
                assignedGiftcouponData.save(function(err,assignedGiftcouponDataResult){
                    if(err){
                        res.status(400).send({success: false, error : err});
                    }
                    if(assignedGiftcouponDataResult){
                        console.log(assignedGiftcouponDataResult);
                        assignedCouponId = assignedGiftcouponDataResult.id;
                        //update transaction history data 
                        TransactionHistory.transactionHistory.update({ _id: transactionID} ,
                            { $set : {  paymentStatus : 'success',
                                        assignGiftcouponId : assignedCouponId,                   
                                        updated_at : new Date() } }, function (err, transData){
                                if(err){
                                    res.json({ success: false, error : err });
                                }else{
                                    TransactionHistory.rewardPonisMaster.update({transactionHistory_id : transactionID, user_id : userId} ,
                                { $set : {  paymentStatus : 'success',
                                            updated_at : new Date() } }, function (err, data){
                                        if(err){
                                            res.json({ success: false, error : err });
                                        }else{

                                            registerModel.find({ _id : userId},{Email : 1, RegisterType : 1, First_name : 1, Last_name : 1}).exec(function(err, userData){
                                                if(err){
                                                    res.json({ success: false, error : err });
                                                }else if(userData && userData.length>0){
                                                   
                                                    userData[0].Email
                
                                                    if(userData[0].RegisterType === "email"){
                
                                                        let  messageData = '<html>\
                                                                <body>\
                                                                    <p> Hello '+ userData[0].First_name+" "+ userData[0].Last_name + '</p>\
                                                                    <h3>Welcome To Panel Reward.</h3>\
                                                                    <p> Hope You are doing great! <br/>\
                                                                    <p> Your requested for Giftcoupon has been aggigned by admin , Please check their on your tranaction history <br/>\
                                                                    <p>Total point  : '+ totalPoints +'</p>\
                                                                    <p>Total amount : '+ req.body.totalAmount +'</p>\
                                                                    <p>Payment type : '+ req.body.totalAmount +'</p>\
                                                                    <p>Thanks <br/>\
                                                                    PANEL REWARD TEAM<p>\
                                                                    <p>Note : please do not reply on this email.</p>\
                                                                </body>\
                                                            </html>'
                                                           let userEmail = hash.decrypt(userData[0].Email);
                                                           let subjectMail = "Giftcoupon Assigned"
                                                        sendMail(userEmail, subjectMail, messageData, function (msg) {
                                                            if (msg) {
                                                                res.status(200).send({ success: true,  message: 'Your tranaction request has been successfully assign to the admin' });
                                                            }
                                                        });
                                                    }else{
                                                        res.json({ success: false, error : 'Please update your email in profile' });
                                                    }
                                                }else{
                                                    res.json({ success: false, error : 'User not found in database' });
                                                }
                                                
                                            });
                                            //res.status(200).send({ success: true, message : 'Your transaction has been complete.'  });
                                        }
                                    });
                                }
                            }); 
                    }
                });
            }
           })
        }else{
            res.json({ success: false, error : 'You request for wrong payment' });
        }
    });    
}

//....send mail function
function sendMail(email, subjectMail, messageData,  callback) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'support@panelreward.com', // sender address
        to: email, // list of receivers
        subject: subjectMail,
        html: messageData
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            callback(error);
            console.log(error);
            //res.status(404).send({ success: false, error });
        }
        if (info) {
            console.log('Message sent: %s', info.messageId);
            var respo = ('Message sent: %s', info.messageId);
            callback(respo);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            //res.status(200).send({ success: true, message: " The link has been sent on your email " });

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }

    });
}

function getALLTransactionID(arrayCoupon, transData){
    let Options = [];      
        let tempPromise=[];
    for (key in arrayCoupon) { 
        
        let couponAmount = arrayCoupon[key].split(":")[0];
        let coupontCount = parseInt(arrayCoupon[key].split(":")[1]);

        // Return new promise 
        tempPromise.push( new Promise(function(resolve, reject) {
            var d = new Date();
            d.setDate(d.getDate()-1);
            let newDate = d.toISOString();
            // Do async job
            coupon.giftCoupon.find({"Coupon_amount" : couponAmount, "Coupon_status" : "active", Expire_date: { $gte: newDate} },{_id: 1}).limit(coupontCount).exec(function(err, giftCoupon20Data){
                
                // Options[key] =  giftCoupon20Data;
                // resolve(Options[key]);
                for(var j = 0; j < giftCoupon20Data.length; j++){
                    Options.push(giftCoupon20Data[j].id);    
                }
                resolve(Options);
            });     
        }));
    }

    Promise.all(tempPromise)
    .then(values => {   
        transData( values[0]);
    });
}


function updateALLTransactionID(couponIdData, transUpdate ){
    let Options = [];      
    let tempPromise=[];
for (key in couponIdData) { 
    
    let couponId = couponIdData[key];
   

    // Return new promise 
    tempPromise.push( new Promise(function(resolve, reject) {

        // Do async job
        coupon.giftCoupon.update({ _id: couponId } ,{ $set : { Coupon_status :  "used" , updatedAt : new Date()} }, function (err, data){
            Options[key] =  data;
            resolve(Options[key]);
           
        });     
    }));
}

Promise.all(tempPromise)
.then(values => {   
    transUpdate( values);
});
}

function getGiftcouponByUser(req, res){
    let couponArray = []
    coupon.assignedGiftcoupon.find({"_id" : req.params.assignId}).exec(function(err, assignCouponData){

        if(err){
            res.json({ success: false, error : err });
        }
        if(assignCouponData){
            couponArray.push(assignCouponData[0].couponId)

            //getCouponDetails  -- couponCode
            getCouponDetails(couponArray[0] ,function(couponDetails){
                
                if(couponDetails.length>0){
                    res.status(200).send({ success: true, couponDetails });

                }else{
                    res.json({ success: false, error : "Giftcoupons not founds" });
                }
            });
        }
    });
}

function getCouponDetails(couponArray, couponData){
    let Options = [];      
        let tempPromise=[];
    for ( var i=0; i<couponArray.length; i++) { 
        
        let couponId = couponArray[i];

        // Return new promise 
        tempPromise.push( new Promise(function(resolve, reject) {

            // Do async job
            coupon.giftCoupon.find({_id: couponId }).exec(function(err, giftCouponsData){
                
                Options.push(giftCouponsData);    
               
                resolve(Options);
            });     
        }));
    }

    Promise.all(tempPromise)
    .then(values => {   
        couponData( values[0]);
    });
}


module.exports = {
    addCoupons : addCoupons,
    allCouponByAdmin : allCouponByAdmin,
    getAllCouponCountByAdmin : getAllCouponCountByAdmin,
    assignGiftcouponOnline : assignGiftcouponOnline,
    getGiftcouponByUser : getGiftcouponByUser
};