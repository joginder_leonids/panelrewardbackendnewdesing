var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

    var registerModel = new Schema({
        RegisterType : {type: String},
        FacebookID : {type: String},
        GoogleID : {type: String},
        ImageURL : {type: String},
        ImageStatus : {type: Boolean, default: false },
        Email: { type: String },
        Contact: { type: String },
        Password:{type: String},
        First_name  : {type: String},
        Last_name  : {type: String},
        D_O_B  : {type: String},
        Gender  :   {type: String},
        Address_line_one : {type: String},
        Address_line_two :  {type: String},
        Country :   {type: String},
        State :   {type: String},
        Pincode :   {type: String},
        Marriage_status : {type: String},
        Ip_address : {type: String},
        Token : {type: String},
        Is_Active  : {type : Boolean},
    
        DeviceType : {type: String},
        DeviceToken : {type: String},
        DeviceID  : {type : String},
        RegisterStep : {type: String},
        createdAt:  {type: Date },
        updatedAt:  {type: Date}
});


module.exports = mongoose.model('register_users',registerModel);