const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        

// --------- Router --------

        //----- Register Route -------//
        mappingRouter.route('/register-user')
        .post(mappingController.create)
     
        //----  varifyToken -------//
        mappingRouter.route('/varifyToken')
        .post(mappingController.varifyToken)

        mappingRouter.route('/getAllUserProfile/:token')
        .get(mappingController.getAllUser)

        //----- Register Route Mobile App -------//
        mappingRouter.route('/register_mobile_primary')
        .post(mappingController.register_mobile_primary)

        mappingRouter.route('/register_mobile_secondary')
        .post(mappingController.register_mobile_secondary)

        mappingRouter.route('/register_mobile_final')
        .post(mappingController.register_mobile_final)


        //------------------- Admin routes -----------------------//

        mappingRouter.route('/getUserCountByCountry')
        .get(mappingController.getUserCountByCountry)

        mappingRouter.route('/getUsersByCountry/:country/:startPoint')
        .get(mappingController.getUsersByCountry)
       
module.exports = mappingRouter;