
// function for handel response for request processor

function SuccessResponse(result){
    this.success = true;
    if (typeof result === 'string') 
        this.message = result;
    else 
        this.result = result;
}

module.exports = SuccessResponse;