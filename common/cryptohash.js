var crypto = require('crypto');
let algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

// encrypted the text
  exports.encrypt =function(text, result){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return  crypted;
  }
   
// decrypted the text  
  exports.decrypt =function(text, result){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
  }