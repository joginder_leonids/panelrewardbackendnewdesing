// Dependency files
const successResponse = require('./responseHandler'),
    errorResponse = require('./errorHandler');

// Compute the request and provide response

function requestProcessor(fn) {
    return function (request, response) {
        let result;
        switch (request.method.toUpperCase()) {
            case 'POST':
                result = fn(request.body);
                break;
            case 'PUT':
                result = fn(request.params, request.body);
                break;
            case 'GET':
                result = fn(request.params);

        }
        if (result.then) {
            result.then(                
                (result) => response.json(new successResponse(result)),
                (err) => response.json(errorResponse(err))
            );
        }
        else response.json(result);
    }
}
module.exports = requestProcessor;