const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
      
        

// --------- Router --------

//Login user 

/*=============================================
=          GET DASHBOARD COUNT DATA          =
=============================================*/
mappingRouter.route('/getDashboardData/:token')
        .get(mappingController.getDashboardData)
        

module.exports = mappingRouter;
