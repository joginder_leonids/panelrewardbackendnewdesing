var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

var categoryModel = new Schema({
  
    catName     :  {type : String},
    order_no    :  {type : String},
    cat_id      :  {type : String},
    isActive    :  {type : Boolean},
    created_at  :  {type : Date },
    updated_at  :  {type : Date}
});

var subCategoryModel = new Schema({
  
    catName     : {type : String},
    isActive    : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var questionsModel = new Schema({
    
    orderQues : {type : Number},
    ques_id   : {type : String},
    cat_id    : {type : String},
    questions   : {type : String},
    selectionType  : {type : String },
    sub_ques : {type : Boolean},
    status  : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var subQuestionsModel = new Schema({
    
    orderSubQues : {type : Number},
    sub_ques_id :{type : String},
    ques_id   : {type : String},
    cat_id   : {type : String},
    questions   : {type : String},
    selectionType  : {type : String },
    sec_sub_ques : {type : Boolean},
    status  : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var SecondSubQuestionsModel = new Schema({
    
    orderSecondSubQues : {type : Number},
    sec_sub_ques_id :{type : String},
    sub_ques_id :{type : String},
    ques_id   : {type : String},
    cat_id   : {type : String},
    questions   : {type : String},
    selectionType  : {type : String },
    status  : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var optionModel = new Schema({
    
    option_id   : {type : String},
    ques_id   : {type : String},
    options  : {type : String },
    status  : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var subOptionModel = new Schema({
    
    option_id   : {type : String},
    sub_ques_id : {type : String},
    ques_id   : {type : String},
    options  : {type : String },
    status  :  {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var secondSubOptionModel = new Schema({
    
    option_id   : {type : String},
    sec_sub_ques_id : {type : String},
    sub_ques_id : {type : String},
    ques_id   : {type : String},
    options  : {type : String },
    status  :  {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var userAnswerModel = new Schema({

    user_id  : {type : String },
    ques_id   : {type : String},
    sub_ques_id : {type : String},
    sec_sub_ques_id : {type : String},
    cat_id   : {type : String},
    option_id   : {type : String},
    optionArr :  { type : Array , default : [] },  //array of user answers
    sub_ques : {type : Boolean},
    sec_sub_ques : {type : Boolean},
    ansType : {type : String},                     // checkbox or select 
    status  : {type : Boolean},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var category =  mongoose.model('profile_category', categoryModel);
var subCategory =  mongoose.model('profile_subCategory', subCategoryModel);
var questions =  mongoose.model('questions', questionsModel);
var sub_questions =  mongoose.model('sub_questions', subQuestionsModel);
var second_sub_questions =  mongoose.model('second_sub_questions', SecondSubQuestionsModel);
var options =  mongoose.model('options', optionModel);
var sub_options =  mongoose.model('sub_options', subOptionModel);
var second_sub_options =  mongoose.model('second_sub_options', secondSubOptionModel);
var userAnswers =  mongoose.model('user_answers', userAnswerModel);




module.exports = {
    category : category,
    subCategory : subCategory,
    questions : questions,
    sub_questions : sub_questions,
    second_sub_questions : second_sub_questions,
    options : options,
    sub_options : sub_options,
    second_sub_options : second_sub_options,
    userAnswers: userAnswers
};