
var categoryModel = require('./Model');
var category = categoryModel.category;
var questions = categoryModel.questions;
var subQuestions = categoryModel.sub_questions;
var secSubQuestion = categoryModel.second_sub_questions;
var options = categoryModel.options;
var subOptions = categoryModel.sub_options;
var secondSubOption = categoryModel.second_sub_options;
var userAnswer = categoryModel.userAnswers;

//Save register

function createCategory(req,res){

    let categoryData = new category({
        cat_id :  req.body.catID,
        catName   :  req.body.categoryName,
        isActive     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    categoryData.save(function(err,categoryResultData){
        res.status(200).send({ success: true, categoryResultData });
	});
}

function getAllProfileCategory(req, res){
    let user_id = req.params.uId;
    category.find({}).sort({order_no : 1}).exec(function(err, Data){
        let str=JSON.stringify(Data);
        let catData = JSON.parse(str);
        if(catData.length>0){
            gettotalQuestionByCat(catData ,function(totalQuestionByCat){
                gettotalQuestionBySubCat(catData ,function(totalQuestionBySubCat){
                    // for(var i=0; i<totalQuestionBySubCat.length; i++){
                    //     totalQuestionByCat.push(totalQuestionBySubCat[i]);
                    // }
                    
                  //  gettotalQuestionBySecSubCat(catData ,function(totalQuestionBySecSubCat){
                        // for(var i=0; i<totalQuestionBySecSubCat.length; i++){
                        //     totalQuestionByCat.push(totalQuestionBySecSubCat[i]);
                        // }

                        gettotalAnsByCat(catData ,user_id ,function(totalAnsByCat){
                           
                            let totalper = [];
                            let total = "";
                        for(var i =0; i<totalQuestionByCat.length; i++){
                             //   totalper[i] =  Math.round((totalAnsByCat[i] * 100)/(totalQuestionByCat[i]+ totalQuestionBySubCat[i]+ totalQuestionBySecSubCat[i]));
                             totalper[i] =  Math.round((totalAnsByCat[i] * 100)/(totalQuestionByCat[i]+ totalQuestionBySubCat[i]));
                            }
                        
                        for(var i =0; i<catData.length; i++){
                            catData[i].percentage = totalper[i];
                        }
                        res.status(200).send({ success: true, categoryData : catData });
                        });
                    });
                });
          //  });   
        }
	});
}




function gettotalQuestionByCat(catData, totalQuestionByCat){

    let TotalQuestion = [];
    let tempPromise=[];
    
    for (key in catData) {
    
      CatID = catData[key].cat_id;
       
      // Return new promise 
       tempPromise.push( new Promise(function(resolve, reject) {
  
      // Do async job
      questions.find({cat_id : CatID }).count(function(err, count){
        
        TotalQuestion[key] =  count;
        //TotalQuestion[key].cat_id = CatID;
        resolve(TotalQuestion[key]);
      });     
    }));
  }

  Promise.all(tempPromise)
  .then(values => { 
    totalQuestionByCat ( values);
});
}


function gettotalQuestionBySubCat(catData, totalQuestionByCat){

    let TotalQuestion = [];
    let tempPromise=[];
    
    for (key in catData) {
    
      CatID = catData[key].cat_id;
       
      // Return new promise 
       tempPromise.push( new Promise(function(resolve, reject) {
  
      // Do async job
      subQuestions.find({cat_id : CatID }).count(function(err, count){
        
        TotalQuestion[key] =  count;
        //TotalQuestion[key].cat_id = CatID;
        resolve(TotalQuestion[key]);
      });     
    }));
  }

  Promise.all(tempPromise)
  .then(values => { 
    totalQuestionByCat ( values);
});
}

//gettotalQuestionBySecSubCat
function gettotalQuestionBySecSubCat(catData, totalQuestionByCat){

    let TotalQuestion = [];
    let tempPromise=[];
    
    for (key in catData) {
    
      CatID = catData[key].cat_id;
       
      // Return new promise 
       tempPromise.push( new Promise(function(resolve, reject) {
  
      // Do async job
      secSubQuestion.find({cat_id : CatID }).count(function(err, count){
        
        TotalQuestion[key] =  count;
        //TotalQuestion[key].cat_id = CatID;
        resolve(TotalQuestion[key]);
      });     
    }));
  }

  Promise.all(tempPromise)
  .then(values => { 
    totalQuestionByCat ( values);
});
}

function gettotalAnsByCat(catData, user_id, totalAnsByCat){

    let TotalAns = [];
    let tempPromise=[];
    
    for (key in catData) {
    
      CatID = catData[key].cat_id;
       
      // Return new promise 
       tempPromise.push( new Promise(function(resolve, reject) {
  
      // Do async job
      userAnswer.find({cat_id : CatID, user_id: user_id}).count(function(err, count){
        
        TotalAns[key] =  count;
        
        resolve(TotalAns[key]);
      });     
    }));
  }

  Promise.all(tempPromise)
  .then(values => { 
    totalAnsByCat ( values);
});
}


function insertQuestion(req,res){

    let sub_ques = false;
    if(req.body.sub_ques){
        sub_ques = req.body.sub_ques;
    }

    let questionsData = new questions({
        orderQues : req.body.orderQues,
        ques_id :  req.body.ques_id,
        cat_id   :  req.body.cat_id,
        questions : req.body.questions,
        selectionType : req.body.selectionType,
        sub_ques : sub_ques,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    questionsData.save(function(err,data){
        res.status(200).send({ success: true, question : data });
	});
}


function insertSubQuestion(req, res){

    let sec_sub_ques = false;
    if(req.body.sec_sub_ques){
        sec_sub_ques = req.body.sec_sub_ques;
    }

    let subQuestionsData = new subQuestions({
        orderSubQues : req.body.orderSubQues,
        sub_ques_id : req.body.sub_ques_id,
        ques_id :  req.body.ques_id,
        cat_id   :  req.body.cat_id,
        questions : req.body.questions,
        selectionType : req.body.selectionType,
        sec_sub_ques : sec_sub_ques,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    subQuestionsData.save(function(err,data){
        res.status(200).send({ success: true, question : data });
	});
}

//insertSecondSubQuestion
function insertSecondSubQuestion(req, res){

    let secSubQuestionData = new secSubQuestion({
        orderSecondSubQues : req.body.orderSecondSubQues,
        sec_sub_ques_id : req.body.sec_sub_ques_id,
        sub_ques_id :  req.body.sub_ques_id,
        ques_id :  req.body.ques_id,
        cat_id   :  req.body.cat_id,
        questions : req.body.questions,
        selectionType : req.body.selectionType,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    secSubQuestionData.save(function(err,data){
        res.status(200).send({ success: true, question : data });
	});
}

function insertOptions(req,res){

    let optionsData = new options({
        option_id :  req.body.option_id,
        ques_id   :  req.body.ques_id,
        options : req.body.options,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    optionsData.save(function(err,data){
        res.status(200).send({ success: true, option : data });
	});
}

//insertSubOptions
function insertSubOptions(req,res){

    let subOptionsData = new subOptions({
        option_id :  req.body.option_id,
        sub_ques_id : req.body.sub_ques_id,
        ques_id   :  req.body.ques_id,
        options : req.body.options,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    subOptionsData.save(function(err,data){
        res.status(200).send({ success: true, option : data });
	});
}

//insertSecondSubOptions
function insertSecondSubOptions(req,res){

    let secondSubOptionData = new secondSubOption({
        option_id :  req.body.option_id,
        sec_sub_ques_id : req.body.sec_sub_ques_id,
        sub_ques_id : req.body.sub_ques_id,
        ques_id   :  req.body.ques_id,
        options : req.body.options,
        status     :  true,
        created_at   :  new Date(),
        updated_at   :  new Date()
    });

    secondSubOptionData.save(function(err,data){
        res.status(200).send({ success: true, option : data });
	});
}

//insertUserAns
function insertUserAns(req,res){

    let categoryId = req.params.catId;
    let userId = req.params.uId;

    if(req.body.sec_sub_ques_id){

        userAnswer.find({cat_id : req.params.catId, 
            user_id : req.params.uId, 
            ques_id : req.body.ques_id,
            sub_ques_id : req.body.sub_ques_id,
            sec_sub_ques_id : req.body.sec_sub_ques_id }).exec(function(err, Data){
                if(Data.length>0){
                    if(req.body.option_id === "---------"){
                        userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sub_ques_id : req.body.sub_ques_id}, function(err, result) {
                    //    res.status(200).send({ success: true,  message: "Ans has been updated. " });
                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                            if(callBackResponse){
                                res.status(200).send({success: true, questions:callBackResponse});
                            }
                        });
                        
                    });
                    }else{
                        userAnswer.update({cat_id : req.params.catId, 
                            user_id : req.params.uId, 
                            ques_id : req.body.ques_id, 
                            sub_ques_id : req.body.sub_ques_id,
                            sec_sub_ques_id : req.body.sec_sub_ques_id } ,{ $set : { option_id : req.body.option_id } }, function (err, data){
                            if(err){
                                res.json({ success:false, message: err });
                            }else{

                                getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                    if(callBackResponse){
                                        res.status(200).send({success: true, questions:callBackResponse});
                                    }
                                });

                               // res.status(200).send({ success: true, data, message: "Ans has been updated. " });
                            }
                        });
                    }
                }else{
                    let userAnswerData = new userAnswer({
                        user_id :   req.params.uId,
                        ques_id :   req.body.ques_id,
                        sub_ques_id : req.body.sub_ques_id,
                        sec_sub_ques_id : req.body.sec_sub_ques_id,
                        cat_id  :   req.params.catId,
                        option_id : req.body.option_id,
                        sec_sub_ques : true,
                        status  :  true,
                        created_at   :  new Date(),
                        updated_at   :  new Date()
                    });
                    userAnswerData.save(function(err,data){
                        //res.status(200).send({ success: true,  data });
                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                            if(callBackResponse){
                                res.status(200).send({success: true, questions:callBackResponse});
                            }
                        });
                    });
                }
            });

    }else if(req.body.sub_ques_id){

        if(req.body.ansType == "checkBox"){
            let mArray = [];
            if(req.body.device === "mobile"){
                let arrayString ="";
                arrayString = req.body.optionArr;
                if(arrayString.match(",")){
                    mArray = req.body.optionArr.split(",");
                }else{
                    mArray = req.body.optionArr;    
                } 
            }else{
                mArray = req.body.optionArr;
            }

            userAnswer.find({cat_id : req.params.catId, 
                user_id : req.params.uId, 
                ques_id : req.body.ques_id,
                sub_ques_id : req.body.sub_ques_id, ansType : req.body.ansType }).exec(function(err, Data){
                    if(Data.length>0){
                        userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sub_ques_id : req.body.sub_ques_id, ansType : req.body.ansType}, function(err, result) {
                           
                            if(mArray.length>0){
                                let userAnswerData = new userAnswer({
                                    user_id :   req.params.uId,
                                    ques_id :   req.body.ques_id,
                                    sub_ques_id : req.body.sub_ques_id,
                                    cat_id  :   req.params.catId,
                                    optionArr : mArray,
                                    sub_ques : true,
                                    ansType : req.body.ansType,
                                    status  :  true,
                                    created_at   :  new Date(),
                                    updated_at   :  new Date()
                                });
                                userAnswerData.save(function(err,data){
                                    //res.status(200).send({ success: true});
                                    getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                        if(callBackResponse){
                                            res.status(200).send({success: true, questions:callBackResponse});
                                        }
                                    });
                                });
                            }else{
                                getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                    if(callBackResponse){
                                        res.status(200).send({success: true, questions:callBackResponse});
                                    }
                                });
                            }    
                        });
                    }else{
                        let userAnswerData = new userAnswer({
                            user_id :   req.params.uId,
                            ques_id :   req.body.ques_id,
                            sub_ques_id : req.body.sub_ques_id,
                            cat_id  :   req.params.catId,
                            optionArr : mArray,
                            sub_ques : true,
                            ansType : req.body.ansType,
                            status  :  true,
                            created_at   :  new Date(),
                            updated_at   :  new Date()
                        });
                        userAnswerData.save(function(err,data){
                            //res.status(200).send({ success: true});
                            getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                if(callBackResponse){
                                    res.status(200).send({success: true, questions:callBackResponse});
                                }
                            });
                        });
                    }
                });

        }else{

            userAnswer.find({cat_id : req.params.catId, 
                user_id : req.params.uId, 
                ques_id : req.body.ques_id,
                sub_ques_id : req.body.sub_ques_id }).exec(function(err, Data){
                    if(Data.length>0){
                        if(req.body.option_id === "---------"){
                            userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sub_ques : true}, function(err, result) {
                            //res.status(200).send({ success: true,  message: "Ans has been updated. " });
                                getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                    if(callBackResponse){
                                        res.status(200).send({success: true, questions:callBackResponse});
                                    }
                                });
                            });
                        }else{

                            userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sec_sub_ques : true}, function(err, result) {
                            if(req.body.option_id === "Select"){
                                userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sub_ques : true}, function(err, result) {
                                    getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                        if(callBackResponse){
                                            res.status(200).send({success: true, questions:callBackResponse});
                                        }
                                    });
                                });
                            }else{
                                userAnswer.update({cat_id : req.params.catId, 
                                    user_id : req.params.uId, 
                                    ques_id : req.body.ques_id, 
                                    sub_ques_id : req.body.sub_ques_id } ,{ $set : { option_id : req.body.option_id } }, function (err, data){
                                    if(err){
                                        res.json({ success:false, message: err });
                                    }else{
                                        //res.status(200).send({ success: true, data, message: "Ans has been updated. " });
                                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                            if(callBackResponse){
                                                res.status(200).send({success: true, questions:callBackResponse});
                                            }
                                        });
                                    }
                                });
                            }  
                            });
                        }
                    }else{
                        let userAnswerData = new userAnswer({
                            user_id :   req.params.uId,
                            ques_id :   req.body.ques_id,
                            sub_ques_id : req.body.sub_ques_id,
                            cat_id  :   req.params.catId,
                            option_id : req.body.option_id,
                            sub_ques : true,
                            status  :  true,
                            created_at   :  new Date(),
                            updated_at   :  new Date()
                        });
                        userAnswerData.save(function(err,data){
                            //res.status(200).send({ success: true,  data });
                            getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                if(callBackResponse){
                                    res.status(200).send({success: true, questions:callBackResponse});
                                }
                            });
                        });
                    }
                });
            }

        

    }else{
        if(req.body.ansType == "checkBox"){
            let mArray = [];
            if(req.body.device=== "mobile"){
                let arrayString ="";
                arrayString = req.body.optionArr;
                if(arrayString.match(",")){
                    mArray = req.body.optionArr.split(",");
                }else{
                    mArray = req.body.optionArr;    
                }
                
            }else{
                mArray = req.body.optionArr;
            }

            userAnswer.find({cat_id : req.params.catId, 
            user_id : req.params.uId, 
            ques_id : req.body.ques_id,
            ansType : req.body.ansType }).exec(function(err, Data){
                if(Data.length>0){
                    userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, ansType : req.body.ansType}, function(err, result) {
                        
                        if(mArray.length>0){
                            let userAnswerData = new userAnswer({
                                user_id :   req.params.uId,
                                ques_id :   req.body.ques_id,
                                cat_id  :   req.params.catId,
                                optionArr : mArray,
                                ansType : req.body.ansType,
                                status  :  true,
                                created_at   :  new Date(),
                                updated_at   :  new Date()
                            });
                            userAnswerData.save(function(err,data){
                                //res.status(200).send({ success: true});
                                getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                    if(callBackResponse){
                                        res.status(200).send({success: true, questions:callBackResponse});
                                    }
                                });
                            });
                        }else{
                            getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                if(callBackResponse){
                                    res.status(200).send({success: true, questions:callBackResponse});
                                }
                            });
                        }
                    })
                }else{
                    let userAnswerData = new userAnswer({
                        user_id :   req.params.uId,
                        ques_id :   req.body.ques_id,
                        cat_id  :   req.params.catId,
                        optionArr : mArray,
                        ansType : req.body.ansType,
                        status  :  true,
                        created_at   :  new Date(),
                        updated_at   :  new Date()
                    });
                    userAnswerData.save(function(err,data){
                        //res.status(200).send({ success: true});
                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                            if(callBackResponse){
                                res.status(200).send({success: true, questions:callBackResponse});
                            }
                        });
                    });
                }
            });

        }else{

            userAnswer.find({cat_id : req.params.catId, 
            user_id : req.params.uId, 
            ques_id : req.body.ques_id }).exec(function(err, Data){

            if(Data.length>0){
                if(req.body.option_id === "---------"){
                    userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId}, function(err, result) {
                    //res.status(200).send({ success: true,  message: "Ans has been updated. " });
                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                            if(callBackResponse){
                                res.status(200).send({success: true, questions:callBackResponse});
                            }
                        });
                    });
                }else{
                    if(req.body.option_id === "Select"){
                        userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId}, function(err, result) {
                            //res.status(200).send({ success: true,  message: "Ans has been updated. " });
                                getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                    if(callBackResponse){
                                        res.status(200).send({success: true, questions:callBackResponse});
                                    }
                                });
                            });
                    }else{
                        userAnswer.remove({user_id :  req.params.uId, ques_id : req.body.ques_id, cat_id : req.params.catId, sub_ques : true}, function(err, result) {
                            userAnswer.update({cat_id : req.params.catId, 
                                user_id : req.params.uId, 
                                ques_id : req.body.ques_id } ,{ $set : { option_id : req.body.option_id } }, function (err, data){
                                if(err){
                                    res.json({ success:false, message: err });
                                }else{
                                    //res.status(200).send({ success: true, data, message: "Ans has been updated. " });
                                    getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                                        if(callBackResponse){
                                            res.status(200).send({success: true, questions:callBackResponse});
                                        }
                                    });
                                }
                            });
                        });
                    }                 
                    }
                }else{
                    let userAnswerData = new userAnswer({
                        user_id :   req.params.uId,
                        ques_id :   req.body.ques_id,
                        cat_id  :   req.params.catId,
                        option_id : req.body.option_id,
                        status  :  true,
                        created_at   :  new Date(),
                        updated_at   :  new Date()
                    });
                    userAnswerData.save(function(err,data){
                        //res.status(200).send({ success: true,  data });
                        getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse){
                            if(callBackResponse){
                                res.status(200).send({success: true, questions:callBackResponse});
                            }
                        });
                    });
                }
            });
        }
    }
}

//getQuestionDataByuser

function getQuestionDataByuser(req, res){
    let categoryId = req.params.catId;
    let userId = req.params.uId;
    getAllQuestionDataByuser( categoryId, userId ,function(callBackResponse, count){
        if(callBackResponse){
            //console.log( callBackResponse.length + " -----  "+ count);

            // userAnswer.find({user_id : user_id, cat_id : category_id},
            //     { ques_id : 1, option_id : 1, optionArr : 1}).count().exec(function(err, userAnsCount){

                    
            //     });
            res.status(200).send({success: true, questions:callBackResponse});
        }
    });
}



function getAllQuestionDataByuser( categoryId, userId , callBackResponse){
   let category_id = categoryId;
   
   let  subQuesCountAgain = 0;
   let user_id = userId;
   let subQuesArray = [];
   let secSubQuesArray = [];
   let secondSubQuesArray = [];
   let quesFinelArray =[];
   questions.find({cat_id : category_id}).exec(function(err, Data){
        let countUserAns = 0;
        let str=JSON.stringify(Data);
        let qusData = JSON.parse(str);

        userAnswer.find({user_id : user_id, cat_id : category_id},
            { ques_id : 1, option_id : 1, optionArr : 1}).exec(function(err, userData){

           
            for(var i = 0; i<qusData.length; i++){
                for(var j = 0; j<userData.length; j++){
                    if(qusData[i].selectionType === "select"){
                        if(qusData[i].ques_id === userData[j].ques_id){ 
                            countUserAns = countUserAns + 1;
                            qusData[i].userOptions = userData[j].option_id;
                            break;
                        }else{
                            qusData[i].userOptions = "0";
                        }
                    }else if(qusData[i].selectionType === "checkBox"){
                        if(qusData[i].ques_id === userData[j].ques_id){ 
                            countUserAns = countUserAns + 1;
                            qusData[i].userOptions = userData[j].optionArr;
                            break;
                        }else{
                            qusData[i].userOptions = [];
                        }
                    }
                        
                }
            }

            getOptionData(qusData ,function(optiondata){

                for(var i = 0; i<qusData.length; i++){
                    qusData[i].options = optiondata[i];
                }
                //res.status(200).send({ success: true, questions : qusData });
                let end = 0;
                let subQuesCount = 0;
                let subQuesLoop = 0;
                for(var j = 0; j<qusData.length; j++){
                    
                    if(qusData[j].sub_ques === true && qusData[j].userOptions === "1"){
                        
                        subQuesCount = subQuesCount+ 1;    // count subqueston
                        //.......................subQuestion.........................//
                        let quesID = qusData[j].ques_id;
                        let catID = qusData[j].cat_id
                        
                        subQuestions.find({ques_id : quesID, cat_id : catID}).exec(function(err, subData){
                        
                            let str=JSON.stringify(subData);
                            let subQuesData = JSON.parse(str);

                            userAnswer.find({user_id : user_id, cat_id : catID, ques_id : quesID, sub_ques :true},
                            { ques_id : 1, sub_ques_id: 1, option_id : 1, optionArr : 1}).exec(function(err, userData){
                                
                                for(var i = 0; i<subQuesData.length; i++){
                                    for(var j = 0; j<userData.length; j++){

                                        if(subQuesData[i].selectionType === "select"){
                                            if(subQuesData[i].sub_ques_id === userData[j].sub_ques_id){ 
                                            
                                                subQuesData[i].userOptions = userData[j].option_id;
                                                break;
                                            }else{
                                                subQuesData[i].userOptions = "0";
                                            }
                                        }else if(subQuesData[i].selectionType === "checkBox"){
                                            if(subQuesData[i].sub_ques_id === userData[j].sub_ques_id){ 
                                            
                                                subQuesData[i].userOptions = userData[j].optionArr;
                                                break;
                                            }else{
                                                subQuesData[i].userOptions = [];
                                            }
                                        }
                                        
                                        
                                    }
                                }
      
                                subQuesLoop = subQuesLoop +1;
                               
                                let countLength = subQuesArray.length;
                                for(var  j=0; j <subQuesData.length; j++){
                                    subQuesArray[countLength+j] = subQuesData[j];
                                }
                                
                                if(subQuesLoop === subQuesCount){
                                    getSubOptionData(subQuesArray ,function(subOptionData){
                                        
                                        for(var i = 0; i<subQuesArray.length; i++){
                                            subQuesArray[i].options = subOptionData[i];
                                        }

                                        let secSubQuesCount = 0;
                                        let secSubQuesLoop =0;
                                        let subEnd = 0;
                                       //...Second Sub Questions......
                                       for(var s = 0; s<subQuesArray.length; s++){
                    
                                            if(subQuesArray[s].sec_sub_ques === true && subQuesArray[s].userOptions === "1"){

                                                secSubQuesCount = secSubQuesCount+ 1;    // count subqueston
                                                //......................second subQuestion.........................//
                                                let quesID = subQuesArray[s].ques_id;
                                                let catID  = subQuesArray[s].cat_id;
                                                let subQuesID =  subQuesArray[s].sub_ques_id;
                                                
                                                secSubQuestion.find({ques_id : quesID, cat_id : catID, sub_ques_id : subQuesID }).exec(function(err, secSubData){

                                                    let str1=JSON.stringify(secSubData);
                                                    let secSubQuesData = JSON.parse(str1);

                                                    userAnswer.find({user_id : user_id, cat_id : catID, ques_id : quesID, sub_ques_id : subQuesID, sec_sub_ques :true},
                                                    { ques_id : 1, sub_ques_id: 1, option_id : 1, sec_sub_ques_id : 1}).exec(function(err, userData){
                                                        
                                                        for(var i = 0; i<secSubQuesData.length; i++){
                                                            for(var j = 0; j<userData.length; j++){
                                                                
                                                                if(secSubQuesData[i].sec_sub_ques_id === userData[j].sec_sub_ques_id){ 
                                                                    
                                                                    secSubQuesData[i].userOptions = userData[j].option_id;
                                                                    break;
                                                                }else{
                                                                    secSubQuesData[i].userOptions = "0";
                                                                }
                                                            }
                                                        }

                                                        //secSubQuesArray

                                                        for(var  j=0; j <secSubQuesData.length; j++){
                                                            secondSubQuesArray.push(secSubQuesData[j]);
                                                        }

                                                        

                    
                                                            secSubQuesLoop = secSubQuesLoop +1;

                                                            if(secSubQuesLoop === secSubQuesCount){

                                                           
                                                        // }
                                                        getSecSubOptionData(secondSubQuesArray ,function(secSubOptionData){
                                        
                                                            for(var i = 0; i<secondSubQuesArray.length; i++){
                                                                secondSubQuesArray[i].options = secSubOptionData[i];
                                                            }

                                                         
                                                                for(var n=0; n<qusData.length ;n++){

                                                                    quesFinelArray.push(qusData[n]);
                
                                                                    if(qusData[n].hasOwnProperty('sub_ques')){
                
                                                                        for(var subQuesBinding = 0; subQuesBinding <subQuesArray.length ; subQuesBinding++)
                                                                        {
                                                                            if(qusData[n].ques_id == subQuesArray[subQuesBinding].ques_id)
                                                                            {
                                                                                quesFinelArray.push(subQuesArray[subQuesBinding]); 
                                                                            }  
                                                                        }                                     
                                                                    }
                                                                }

                                                                let finalListArray = [];
                                                                for(let mm = 0; mm< quesFinelArray.length; mm++)
                                                                {
                                                                    if(quesFinelArray[mm].hasOwnProperty('sec_sub_ques'))
                                                                    {
                                                                        finalListArray.push(quesFinelArray[mm]);
    
                                                                        for(var nn = 0; nn < secondSubQuesArray.length; nn++)
                                                                        {
                                                                            if(quesFinelArray[mm].sub_ques_id == secondSubQuesArray[nn].sub_ques_id)
                                                                            {
                                                                                finalListArray.push(secondSubQuesArray[nn]);    
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        finalListArray.push(quesFinelArray[mm]);
                                                                    }
                                                                }
                                                                callBackResponse(finalListArray,subQuesCountAgain);
                                                            
                                                            //res.status(200).send({ success: true, questions : finalListArray, subQuestions : false,secondSubQuesArray });
                                                        });
                                                    }
                                                    });
                                                });
                                            }else{
                                                subEnd = subEnd+1;
                                                if(subEnd == subQuesArray.length){                                                
                                                   // quesFinelArray
                                                    for(var n=0; n<qusData.length ;n++){

                                                        quesFinelArray.push(qusData[n]);

                                                        if(qusData[n].hasOwnProperty('sub_ques')){

                                                            for(var subQuesBinding = 0; subQuesBinding <subQuesArray.length ; subQuesBinding++)
                                                            {
                                                                if(qusData[n].ques_id == subQuesArray[subQuesBinding].ques_id)
                                                                {
                                                                    quesFinelArray.push(subQuesArray[subQuesBinding]); 
                                                                }
                                                            }                                     
                                                        }
                                                    }
                                                //res.status(200).send({ success: true, questions : quesFinelArray, subQuestions : false });
                                                    callBackResponse(quesFinelArray, subQuesCountAgain);
                                                }
                                            }
                                        }
                                      
                                    });
                                }
                            })
                        });
                    }else{
                        end = end+1;
                        if(end == qusData.length){
                           // res.status(200).send({ success: true, questions : qusData, subQuestions : false });
                           callBackResponse(qusData, subQuesCountAgain);
                        }
                    }
                }
            });
        });
    });
}

//getSecSubOptionData
function getSecSubOptionData(secondSubQuesArray, secSubOptionData){

    let Options = [];
    let tempPromise=[];
    for (key in secondSubQuesArray) {
    
    queID = secondSubQuesArray[key].ques_id;
    subQueID = secondSubQuesArray[key].sub_ques_id;
    secSubQueID = secondSubQuesArray[key].sec_sub_ques_id;
    // Return new promise 
    tempPromise.push( new Promise(function(resolve, reject) {

        // Do async job
        secondSubOption.find({ques_id : queID, sub_ques_id : subQueID, sec_sub_ques_id: secSubQueID}).sort({order_no : 1}).exec(function(err, data){
            
            Options[key] =  data;
            resolve(Options[key]);
        });     
    }));
}

Promise.all(tempPromise)
.then(values => { 
    secSubOptionData ( values);
});
}

function getSubOptionData(subQuesData, subOptionData){

    let Options = [];
    let tempPromise=[];
    for (key in subQuesData) {
    
    queID = subQuesData[key].ques_id;
    subQueID = subQuesData[key].sub_ques_id;
    // Return new promise 
    tempPromise.push( new Promise(function(resolve, reject) {

        // Do async job
        subOptions.find({ques_id : queID, sub_ques_id : subQueID}).sort({order_no : 1}).exec(function(err, data){
            
            Options[key] =  data;
            resolve(Options[key]);
        });     
    }));
}

Promise.all(tempPromise)
.then(values => { 
    subOptionData ( values);
});
}

function getOptionData(qusData, optiondata){

        let Options = [];
        let tempPromise=[];
        for (key in qusData) {
        
        queID = qusData[key].ques_id;
        
        // Return new promise 
        tempPromise.push( new Promise(function(resolve, reject) {
    
            // Do async job
            options.find({ques_id : queID}).sort({order_no : 1}).exec(function(err, data){
                
                Options[key] =  data;
                resolve(Options[key]);
            });     
        }));
    }

    Promise.all(tempPromise)
    .then(values => { 
        optiondata ( values);
    });
}




 module.exports = {createCategory : createCategory, 
    insertQuestion      :   insertQuestion,
    insertSubQuestion   :   insertSubQuestion,
    insertSecondSubQuestion: insertSecondSubQuestion,
    insertOptions       :   insertOptions,
    insertSubOptions    :   insertSubOptions,
    insertSecondSubOptions : insertSecondSubOptions,
    insertUserAns       :   insertUserAns,
    getQuestionDataByuser : getQuestionDataByuser,
    getAllProfileCategory : getAllProfileCategory };

