const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');

// --------- Router --------

/*=============================================
=           GET ROUTES           =
=============================================*/   
    
        mappingRouter.route('/getAllProfileCategory/:uId')
        .get( mappingController.getAllProfileCategory) 

        mappingRouter.route('/getQuestionDataByuser/:catId/:uId')
        .get( mappingController.getQuestionDataByuser)

/*=============================================
=           POST ROUTES            =
=============================================*/  

        mappingRouter.route('/createCategory')
        .post(mappingController.createCategory)

        mappingRouter.route('/insertQuestion')
        .post(mappingController.insertQuestion)

        mappingRouter.route('/insertSubQuestion')
        .post(mappingController.insertSubQuestion)

        mappingRouter.route('/insertSecondSubQuestion')
        .post(mappingController.insertSecondSubQuestion)
       
        mappingRouter.route('/insertOptions')
        .post(mappingController.insertOptions)

        mappingRouter.route('/insertSubOptions')
        .post(mappingController.insertSubOptions)

        mappingRouter.route('/insertSecondSubOptions')
        .post(mappingController.insertSecondSubOptions)

        mappingRouter.route('/insertUserAns/:catId/:uId')
        .post(mappingController.insertUserAns)
       
module.exports = mappingRouter;