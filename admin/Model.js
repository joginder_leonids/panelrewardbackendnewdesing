var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

var adminUserModel = new Schema({
    Email: { type: String },
    Password:{type: String},
    First_name  : {type: String},
    Last_name  : {type: String},
    D_O_B  : {type: String},
    Gender  :   {type: String},
    Address : {type: String},
    Token : {type: String},
    Is_Active  : {type : Boolean},
    createdAt:  {type: Date },
    updatedAt:  {type: Date}
});


module.exports = mongoose.model('admin_user',adminUserModel);