const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        

// --------- Router --------

//----- Register Route -------//
        mappingRouter.route('/admin-user')
        .post(mappingController.create)

        mappingRouter.route('/adminLogin')
        .post(mappingController.adminLogin)
     


       
module.exports = mappingRouter;