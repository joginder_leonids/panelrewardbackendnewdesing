const   express = require('express'),
        mappingRouter = express.Router();
        profileModel = require('../registration/Model');
const fileUpload = require('express-fileupload');
// --------- Router --------

const app = express();

var path = require('path');
var appDir = path.dirname(require.main.filename);

// default options
app.use(fileUpload());
//const upload = multer({ dest: 'public/uploads/' });
//Register Route
mappingRouter.post("/uploadRegisterdUserImage/:id",function(req, res) {
  if (Object.keys(req.files).length == 0) {
    res.status(200).send({ success: false, message: 'No files were uploaded.' });
  }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  //  //sampleFile.mv(  'C:/Shared/PandoTest/GNNMyPanelReward/BackendMyPanelreward/'+filePath, function(err) {
      //sampleFile.mv( '/var/www/testmyapi.panelreward.com/' + filePath, function(err) {
  let sampleFile = req.files.sampleFile;
  let fileName =  req.files.sampleFile.name;
  let filePath = '/public/uploads/'+fileName;

  profileModel.find({_id : req.params.id}).exec(function(err, data){
		if(err){
			res.json({ success:false, message: err });
		}else{
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv( appDir + filePath, function(err) {
      if (err){
        return res.status(200).send(err);
      }else{
        profileModel.update({ _id: req.params.id} ,
            { $set : {  ImageURL : filePath,
                        ImageStatus : true,
                        updatedAt : new Date()  
                      } }, function (err, data){
            if (err) {
              res.status(200).send({ success: false, message: err });
            }else{
              res.status(200).send({ success: true, message : 'profile Image has been updated.', imagePath: filePath });
            }
        });
      }
        //return res.status(500).send(err);
      //res.send('File uploaded!');
    });
    }
  });
});
module.exports = mappingRouter;