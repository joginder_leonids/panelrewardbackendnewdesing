var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

    var userDisabledModel = new Schema({
        user_id : { type: Schema.Types.ObjectId , ref: 'register_users' },
        disable_by_id : {type: String},
        disable_by : {type: String},
        createdAt:  {type: Date },
        updatedAt:  {type: Date}
});


module.exports = mongoose.model('disable_users', userDisabledModel);