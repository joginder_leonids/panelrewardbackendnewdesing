var  mongoose = require('mongoose'),

TransactionHistory = require('./Model'),
admin = require('../admin/Model');
studyModel = require('../study/Model');
paypal = require('../paypal/configure');
registerModel = require('../registration/Model');
hash = require('../common/cryptohash');


//Save register
function addTransaction(req,res){
    let totalsPointMaster = "";
    let totalsPointRemaing = "0"
    let userPaypalEmail = "";
    if(req.body.paypalEmailId){
        userPaypalEmail   = '<p>Payment Email id : '+ req.body.paypalEmailId +'</p>'
    }

    let subjectMail = "";
    if(req.body.paymentType === "paypal"){
        subjectMail = 'Paypal Redeemption Alert';
    }else if (req.body.paymentType === "GiftCouponOnline"){
        subjectMail = 'GiftCoupon Redeemption Alert';
    }else if(req.body.paymentType === "GiftCouponOffline"){
        subjectMail = 'GiftCoupon Redeemption Alert';
    }

    studyModel.rewardPoint.find({user_id : req.body.user_id}).exec(function(err, oldPointsDataCheck){

        if(err){
            res.json({ success: false, error : err });
        }else{
     
   
        let TransactionHistoryData = new TransactionHistory.transactionHistory({
            user_id         :   req.body.user_id,
            totalPoints     :   req.body.totalPoints,
            totalAmount     :   req.body.totalAmount,
            paymentType     :   req.body.paymentType,
            paypalEmailId   :   req.body.paypalEmailId,
            deliveryType    :   req.body.deliveryType,
            addressLineOne  :   req.body.addressLineOne,
            addressLineTwo  :   req.body.addressLineTwo,
            state           :   req.body.state,
            city            :   req.body.city,
            pinCode         :   req.body.pinCode,
            paymentStatus   :   "pending",
            paymentComments :   req.body.paymentComments,
            txn_id          :   req.body.txn_id,
            txn_status      :   req.body.txn_status,
            createdAt       :   new Date(),
            updatedAt       :   new Date()
        });

        TransactionHistoryData.save(function(err,transactionHistoryData){
            
            if(err){
                res.json({ success: false, error : err });
            }else{

                //add tranasction history

                studyModel.rewardPoint.find({user_id : req.body.user_id}).exec(function(err, oldPointsData){

                    if(err){
                        res.json({ success: false, error : err });
                    }else{

                        
                        if(oldPointsData[0].total_reward_point === req.body.totalPoints){
                            totalsPointMaster = oldPointsData[0].total_reward_point;
                        }else{
                            totalsPointMaster =  req.body.totalPoints;
                            totalsPointRemaing = parseInt(oldPointsData[0].total_reward_point)-parseInt(req.body.totalPoints)
                        }

                    let rewardPonisMasterData = new TransactionHistory.rewardPonisMaster({

                        user_id   : req.body.user_id,
                        transactionHistory_id : transactionHistoryData.id,
                        total_reward_point : totalsPointMaster,
                        paymentStatus : transactionHistoryData.paymentStatus,
                        paymentType : transactionHistoryData.paymentType,
                        created_at  : new Date(),
                        updated_at  : new Date()
                    });

                    rewardPonisMasterData.save(function(err,rewardMasterData){
            
                        if(err){
                            res.json({ success: false, error : err });
                        }else{
                            
                            studyModel.rewardPoint.update({ user_id: req.body.user_id} ,
                                    { $set : { total_reward_point : totalsPointRemaing,
                                        updated_at : new Date() } }, function (err, data){
            
                            
                                registerModel.find({ _id : req.body.user_id},{Email : 1, RegisterType : 1, First_name : 1, Last_name : 1}).exec(function(err, userData){
                                    if(err){
                                        res.json({ success: false, error : err });
                                    }else if(userData && userData.length>0){
                                    
                                        userData[0].Email

                                        if(userData[0].RegisterType === "email"){

                                            let  messageData = '<html>\
                                                    <body>\
                                                        <p> Hello '+ userData[0].First_name+" "+ userData[0].Last_name + '</p>\
                                                        <h3>Welcome To Panel Reward.</h3>\
                                                        <p> Hope You are doing great! <br/>\
                                                        <p> Your request for redeem points has been accepted and assign to admin for process <br/>\
                                                        <p>Total point  : '+ req.body.totalPoints +'</p>\
                                                        <p>Total amount : '+ req.body.totalAmount +'</p>\
                                                        <p>Payment type : '+ req.body.paymentType +'</p>\
                                                        '+userPaypalEmail+' \
                                                        <p>Thanks <br/>\
                                                        PANEL REWARD TEAM<p>\
                                                        <p>Note : please do not reply on this email.</p>\
                                                    </body>\
                                                </html>'
                                            let userEmail = hash.decrypt(userData[0].Email);
                                            sendMail(userEmail, subjectMail, messageData, function (msg) {
                                                if (msg) {
                                                    res.status(200).send({ success: true,  message: 'Your tranaction request has been successfully assign to the admin' });
                                                }
                                            });
                                        }else{
                                            res.json({ success: false, error : 'Please update your email in profile' });
                                        }
                                    }else{
                                        res.json({ success: false, error : 'User not found in database' });
                                    }
                                    
                                });

                            });
                        }});
                    }
                }) 
            }   
        });
    }
    });
}

//....send mail function
function sendMail(email, subjectMail, messageData,  callback) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'support@panelreward.com', // sender address
        to: email, // list of receivers
        subject: subjectMail,
        html: messageData
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            callback(error);
            console.log(error);
            //res.status(404).send({ success: false, error });
        }
        if (info) {
            console.log('Message sent: %s', info.messageId);
            var respo = ('Message sent: %s', info.messageId);
            callback(respo);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            //res.status(200).send({ success: true, message: " The link has been sent on your email " });

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }

    });
}

function fetchUserTraction(req, res){
    TransactionHistory.transactionHistory.find({user_id : req.params.id}).sort( { updatedAt : -1 } ).exec(function(err, data){
        if(err){
            res.json({success: false, error : err });
        }else{
            res.status(200).send({ success: true, transactionData : data });
        }
    });
}

function collectAllPaymentTransaction(req, res){

    let allTransArray = [];
    admin.find({Token : req.params.token},function(err, success){
        if(success.length>0){
           
            TransactionHistory.transactionHistory.aggregate(
                [
                    { 
                        "$project" : { 
                            "trans" : "$$ROOT"
                        }
                    }, 
                    { 
                        "$lookup" : {
                            "localField" : "trans.user_id", 
                            "from" : "register_users", 
                            "foreignField" : "_id", 
                            "as" : "users"
                        }
                    }, 
                    { 
                        "$unwind" : {
                            "path" : "$users", 
                            "preserveNullAndEmptyArrays" : true
                        }
                    }, 
                    
                    { 
                        "$project" : {
                            "users.First_name" : "$users.First_name", 
                            "users.Last_name" : "$users.Last_name", 
                            "users.Email" : "$users.Email", 
                            "trans.transactionId" : "$trans._id",
                            "trans.user_id" : "$trans.user_id", 
                            "trans.totalPoints" : "$trans.totalPoints", 
                            "trans.totalAmount" : "$trans.totalAmount", 
                            "trans.paymentType" : "$trans.paymentType", 
                            "trans.paypalEmailId" : "$trans.paypalEmailId", 
                            "trans.paymentStatus" : "$trans.paymentStatus", 
                            "trans.txn_id" : "$trans.txn_id", 
                            "trans.txn_status" : "$trans.txn_status", 
                            "trans.createdAt" : "$trans.createdAt", 
                            "trans.updatedAt" : "$trans.updatedAt"
                        }
                    },

                    { "$sort" : { "trans.updatedAt" : -1 } }
                ]
            ).exec(function(err, data){
                if(err){
                    res.json({success: false, error: err});
                }else if(data.length>0){
                    res.status(200).send({success: true,  userTransactionList: data});
                }else{
                    res.json({success: false, message: "Transaction payment  not found "});
                }
            });
        }
    });
}

function collectPaymentTransactionByUser(req, res){

    let tranasctions= {};
    admin.find({Token : req.params.token},function(err, success){
        if(err){
            res.json({success: false, error: err});
        }
        if(success.length>0){
        TransactionHistory.transactionHistory.find({_id : req.params.tId}).exec(function(err, data){
            if(err){
                res.json({ success: false, error : err });
            }else{

                TransactionHistory.transactionHistory.find({user_id : data[0].user_id, paymentStatus :"success"}).sort( { updatedAt : -1 } ).exec(function(err, successData){
                 
                    tranasctions.currentPay = data[0];
                    tranasctions.successPay = successData[0];
                    res.status(200).send({ success: true, transactionData : tranasctions  });
                });
            }
        });
        }else{
            res.json({ success: false, error : 'Not valid admin user' });
        }
    });
}

function verifyPaypalTranaction(req, res){


    admin.find({Token : req.body.token},function(err, successAdmin){
        if(err){
            res.json({success: false, error: err});
        }else if(successAdmin && successAdmin.length){
        
        TransactionHistory.rewardPonisMaster.find({transactionHistory_id : req.body.transactionID, user_id : req.body.userID}).exec(function(err, masterData){
            if(err){
                res.json({ success: false, error : err });
            }else{
                
                if(masterData && masterData.length > 0){

                    if(masterData[0].total_reward_point === req.body.totalPoints){

                        if(parseInt(req.body.totalAmount) === parseInt(masterData[0].total_reward_point/50)){

                            TransactionHistory.transactionHistory.update({ _id: req.body.transactionID} ,
                                { $set : {  paymentStatus : 'approved',
                                            paymentComments : req.body.comment,
                                            checkByAdminID : successAdmin[0]._id,
                                            checkByAdminName : successAdmin[0].First_name +" "+successAdmin[0].Last_name, 
                                            updated_at : new Date() } }, function (err, transData){
                                        if(err){
                                            res.json({ success: false, error : err });
                                        }else{
                                            TransactionHistory.rewardPonisMaster.update({transactionHistory_id : req.body.transactionID, user_id : req.body.userID} ,
                                        { $set : {  paymentStatus : 'approved',
                                            checkByAdminID : successAdmin[0]._id,
                                            checkByAdminName : successAdmin[0].First_name +" "+successAdmin[0].Last_name, 
                                            updated_at : new Date() } }, function (err, data){
                                                if(err){
                                                    res.json({ success: false, error : err });
                                                }else{
                                                    res.status(200).send({ success: true, message : 'varified user' , data : transData});
                                                }
                                            });
                                        }
                                });
                        }else{
                            res.json({ success: false, error : 'Varification failed please reject the user' });
                        }
                    }else{
                        res.json({ success: false, error : 'Varification failed please reject the user' });
                    }
                }else{
                    res.json({ success: false, error : 'Varification failed please reject the user' });
                }
            }
        });
    }else{
        res.json({ success: false, error : 'Not valid admin user' });
    }
    });
}

function rejectPaypalTransaction(req, res){
    let totalRejectPoint = "";
    let paymentRejectType = "";
    let subjectMail = "Redeemption Tranaction Alert";
    let rejectComment = req.body.comment;
    admin.find({Token : req.body.token},function(err, successAdmin){
        if(err){
            res.json({success: false, error: err});
        }else if(successAdmin && successAdmin.length>0){

            TransactionHistory.rewardPonisMaster.find({transactionHistory_id : req.body.transactionID, user_id : req.body.userID}).exec(function(err, masterData){
            if(err){
                res.json({ success: false, error : err });
            }else{
                if(masterData && masterData.length>0){
                    totalRejectPoint = masterData[0].total_reward_point;
                    paymentRejectType = masterData[0].paymentType;
                    studyModel.rewardPoint.find({user_id: req.body.userID}).exec(function(err, remainRewardPoint){

                        let totalpoint = "";
                        if(remainRewardPoint.length>0){
                            
                            totalpoint = parseInt(remainRewardPoint[0].total_reward_point) + parseInt(masterData[0].total_reward_point)
                        }else{

                            totalpoint = masterData[0].total_reward_point;
                        }
                    //
                        studyModel.rewardPoint.update({ user_id: req.body.userID } ,{ $set : { total_reward_point : totalpoint } }, function (err, data){
                            if(err){
                                res.json({ success:false, message: err });
                            }else{
                            TransactionHistory.transactionHistory.update({ _id: req.body.transactionID} ,
                                { $set : {  paymentStatus : 'rejected',
                                            paymentComments : req.body.comment,
                                            checkByAdminID : successAdmin[0]._id,
                                            checkByAdminName : successAdmin[0].First_name +" "+successAdmin[0].Last_name, 
                                            updated_at : new Date() } }, function (err, transData){
                                        if(err){
                                            res.json({ success: false, error : err });
                                        }else{
                                            TransactionHistory.rewardPonisMaster.update({transactionHistory_id : req.body.transactionID, user_id : req.body.userID} ,
                                        { $set : {  paymentStatus : 'rejected',
                                            checkByAdminID : successAdmin[0]._id,
                                            checkByAdminName : successAdmin[0].First_name +" "+successAdmin[0].Last_name, 
                                            updated_at : new Date() } }, function (err, data){
                                                if(err){
                                                    res.json({ success: false, error : err });
                                                }else{

                                                    // reaject mail process
                                                    registerModel.find({ _id : req.body.userID},{Email : 1, RegisterType : 1, First_name : 1, Last_name : 1}).exec(function(err, userData){
                                                        if(err){
                                                            res.json({ success: false, error : err });
                                                        }else if(userData && userData.length>0){
                                                                                 
                                                            if(userData[0].RegisterType === "email"){
                        
                                                                let  messageData = '<html>\
                                                                        <body>\
                                                                            <p> Hello '+ userData[0].First_name+" "+ userData[0].Last_name + '</p>\
                                                                            <h3>Welcome To Panel Reward.</h3>\
                                                                            <p> Hope You are doing great! <br/>\
                                                                            <p> Your requested has been rejected by admin, Please redeem again  </p>\
                                                                            <p>Cancellation reason : ' + rejectComment +'</p>\
                                                                            <p>Total point  : '+ totalRejectPoint +'</p>\
                                                                            <p>Total amount : '+ (parseInt(totalRejectPoint)/50) +' USD</p>\
                                                                            <p>Payment type : '+ paymentRejectType +'</p>\
                                                                            <p>Thanks <br/>\
                                                                            PANEL REWARD TEAM<p>\
                                                                            <p>Note : please do not reply on this email.</p>\
                                                                        </body>\
                                                                    </html>'
                                                                   let userEmail = hash.decrypt(userData[0].Email);
                                                                sendMail(userEmail, subjectMail, messageData, function (msg) {
                                                                    if (msg) {
                                                                        res.status(200).send({ success: true, message : 'Tranaction rejected successfully' });
                                                                    }
                                                                });
                                                            }else{
                                                                res.json({ success: false, error : 'Please update your email in profile' });
                                                            }
                                                        }else{
                                                            res.json({ success: false, error : 'User not found in database' });
                                                        }       
                                                    });
                                                }
                                            });
                                        }
                                });                
                            }
                        });
                        //
                    });
                }
            }
            });
        }else{
            res.json({ success: false, error : 'Not valid admin user' });
        }
    });
}

function payWithPaypal (req, res){

    let paypalEmailID = req.body.paypalEmailId;
    let transactionID = req.body.transactionID;
    let userID = req.body.userID;
    let totalAmount =  req.body.totalAmount;

    var sender_batch_id = Math.random().toString(36).substring(9);

    var create_payout_json = {
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [
            
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": totalAmount,
                    "currency": "USD"
                },
                "receiver": paypalEmailID,
                "note": "Thank you.",
                "sender_item_id": "item_2"
            }
        ]
    };
    
       paypal.payout.create(create_payout_json,true, function (error, payout) {
           if (error) {
                res.json({ success: false, error : error });
           } else {
               
               let paypalPayoutResponse ={}
               paypalPayoutResponse.payoutTxnId = payout.batch_header.payout_batch_id;
               paypalPayoutResponse.payoutTxnStatus = 'processing';
               paypalPayoutResponse.totalAmount = totalAmount;

               TransactionHistory.transactionHistory.update({ _id: transactionID} ,
                { $set : {  paymentStatus : 'processing',
                            txn_id : paypalPayoutResponse.payoutTxnId, 
                            txn_status :  payout.batch_header.payout_batch_status,                         
                            updated_at : new Date() } }, function (err, transData){
                    if(err){
                        res.json({ success: false, error : err });
                    }else{
                        TransactionHistory.rewardPonisMaster.update({transactionHistory_id : transactionID, user_id : userID} ,
                    { $set : {  paymentStatus : 'processing',
                                updated_at : new Date() } }, function (err, data){
                            if(err){
                                res.json({ success: false, error : err });
                            }else{
                                res.status(200).send({ success: true, payout: paypalPayoutResponse });
                            }
                        });
                    }
                });             
           }
       });   
}

function checkpayWithPaypal(req, res){
    
    let paypalEmail = req.body.payaplTxnData.trans.paypalEmailId
    admin.find({Token : req.body.token},function(err, successAdmin){
        if(err){
            res.json({success: false, error: err});
        }else if(successAdmin && successAdmin.length){

            let paypalTxnId = req.body.payaplTxnData.trans.txn_id;
            let transactionID = req.body.payaplTxnData.trans.transactionId;
            let userID = req.body.payaplTxnData.trans.user_id;
            let totalPoint =  req.body.payaplTxnData.trans.totalPoints;
            let totalAmount =  req.body.payaplTxnData.trans.totalAmount;
            let paypalEmailId = req.body.payaplTxnData.trans.paypalEmailId;

        paypal.payout.get(paypalTxnId, function (error, payout) {
            if (error) {
                res.json({ success: false, error : error });
            } else {
                
                if(payout){

                    if(payout.batch_header.batch_status === "SUCCESS"){            

                    //update transaction history and tranaction master
                    let paypalPaystatus ={};
                    paypalPaystatus.transactionStatus = payout.items[0].transaction_status;
                    paypalPaystatus.tranasctionsEmail = payout.items[0].payout_item.receiver;

                    if(paypalPaystatus.transactionStatus === "SUCCESS"){

                        TransactionHistory.transactionHistory.update({ _id: transactionID} ,
                            { $set : {  paymentStatus : 'success',
                                        txn_status :  paypalPaystatus.transactionStatus,                         
                                        updated_at : new Date() } }, function (err, transData){
                                if(err){
                                    res.json({ success: false, error : err });
                                }else{
                                    TransactionHistory.rewardPonisMaster.update({transactionHistory_id : transactionID, user_id : userID} ,
                                { $set : {  paymentStatus : 'success',
                                            updated_at : new Date() } }, function (err, data){
                                        if(err){
                                            res.json({ success: false, error : err });
                                        }else{
                                            // send mail when success payment
                                            registerModel.find({ _id : userID},{Email : 1, RegisterType : 1, First_name : 1, Last_name : 1}).exec(function(err, userData){
                                                if(err){
                                                    res.json({ success: false, error : err });
                                                }else if(userData && userData.length>0){
                                                   
                                                    if(userData[0].RegisterType === "email"){
                
                                                        let  messageData = '<html>\
                                                                <body>\
                                                                    <p> Hello '+ userData[0].First_name+" "+ userData[0].Last_name + '</p>\
                                                                    <h3>Welcome To Panel Reward.</h3>\
                                                                    <p> Hope You are doing great! <br/>\
                                                                    <p> Your transaction has been processed successfully by Paypal</p>\
                                                                    <p>Total point    : '+ totalPoint +'</p>\
                                                                    <p>Total amount   : '+ totalAmount +' USD</p>\
                                                                    <p>Paypal emailId : '+ paypalEmailId +'</p>\
                                                                    <p>Thanks <br/>\
                                                                    PANEL REWARD TEAM<p>\
                                                                    <p>Note : please do not reply on this email.</p>\
                                                                </body>\
                                                            </html>'
                                                           let userEmail = hash.decrypt(userData[0].Email);
                                                           let subjectMail = "Paypal Payment Completed Successfully"
                                                        sendMail(userEmail, subjectMail, messageData, function (msg) {
                                                            if (msg) {
                                                                res.status(200).send({ success: true, payout: paypalPaystatus });
                                                            }
                                                        });
                                                    }else{
                                                        res.json({ success: false, error : 'Please update your email in profile' });
                                                    }
                                                }else{
                                                    res.json({ success: false, error : 'User not found in database' });
                                                }       
                                            });
                                        }
                                    });
                                }
                            });
                    }else if(paypalPaystatus.transactionStatus === "UNCLAIMED"){
                       
                        paypalPaystatus.tranactionFailError = payout.items[0].errors;

                        TransactionHistory.transactionHistory.update({ _id: transactionID} ,
                        { $set : {  paymentStatus : 'unsuccess',
                                    txn_status :  paypalPaystatus.transactionStatus, 
                                    txn_error :   payout.items[0].errors.message,        
                                    updated_at : new Date() } }, function (err, transData){
                            if(err){
                                res.json({ success: false, error : err });
                            }else{
                           
                            TransactionHistory.rewardPonisMaster.find({transactionHistory_id :transactionID, user_id : userID}).exec(function(err, masterData){
                                if(err){
                                    res.json({ success: false, error : err });
                                }else{
                                    if(masterData && masterData.length>0){
                                        studyModel.rewardPoint.update({ user_id: userID } ,{ $set : { total_reward_point : masterData[0].total_reward_point } }, function (err, data){
                                            if(err){
                                                res.json({ success:false, message: err });
                                            }else{

                                            TransactionHistory.rewardPonisMaster.update({transactionHistory_id : transactionID, user_id : userID} ,
                                                { $set : {  paymentStatus : 'unsuccess',
                                                        updated_at : new Date() } }, function (err, data){
                                                    if(err){
                                                        res.json({ success: false, error : err });
                                                    }else{
                                                        // send mail when success payment
                                            registerModel.find({ _id : userID},{Email : 1, RegisterType : 1, First_name : 1, Last_name : 1}).exec(function(err, userData){
                                                if(err){
                                                    res.json({ success: false, error : err });
                                                }else if(userData && userData.length>0){
                                                   
                                                    if(userData[0].RegisterType === "email"){
                
                                                        let  messageData = '<html>\
                                                                        <body>\
                                                                            <p> Hello '+ userData[0].First_name+" "+ userData[0].Last_name + '</p>\
                                                                            <h3>Welcome To Panel Reward.</h3>\
                                                                            <p> Hope You are doing great! <br/>\
                                                                            <p> Your transaction has not been processed by Paypal, please check your paypal emailId</p>\
                                                                            <p>Total point    : '+ totalPoint +'</p>\
                                                                            <p>Total amount   : '+ totalAmount +' USD</p>\
                                                                            <p>Paypal emailId : '+ paypalEmailId +'</p>\
                                                                            <p>Thanks <br/>\
                                                                            PANEL REWARD TEAM<p>\
                                                                            <p>Note : please do not reply on this email.</p>\
                                                                        </body>\
                                                                    </html>'
                                                                let userEmail = hash.decrypt(userData[0].Email);
                                                                let subjectMail = "Paypal Transaction Decline"
                                                                sendMail(userEmail, subjectMail, messageData, function (msg) {
                                                                    if (msg) {
                                                                        res.status(200).send({ success: true, payout: paypalPaystatus });
                                                                    }
                                                                });
                                                            }else{
                                                                res.json({ success: false, error : 'Please update your email in profile' });
                                                            }
                                                        }else{
                                                            res.json({ success: false, error : 'User not found in database' });
                                                        }       
                                                    });
                                                    }
                                                });
                                            }
                                        });
                                    }else{
                                        res.json({ success: false, error : 'transaction master data not found' });
                                    }
                                }
                                });
                            }
                        });
                    }
                }else if(payout.batch_header.batch_status === "PROCESSING"){

                    let paypalPaystatus ={};
                    paypalPaystatus.tranasctionsEmail = paypalEmail;
                    paypalPaystatus.transactionStatus = payout.batch_header.batch_status;
                    paypalPaystatus.paypalTransactionId = payout.batch_header.payout_batch_id;
                    paypalPaystatus.message = "This transaction under processing ",
                    res.status(200).send({ success: true, payout: paypalPaystatus });
                 }
                }
            }
        });
        }else{
            res.json({ success: false, error : 'Not valid admin user' });
        }
    });
}



module.exports = {
    addTransaction      :   addTransaction,
    fetchUserTraction   :   fetchUserTraction,
    collectAllPaymentTransaction    : collectAllPaymentTransaction,
    collectPaymentTransactionByUser : collectPaymentTransactionByUser,
    verifyPaypalTranaction  :  verifyPaypalTranaction,
    rejectPaypalTransaction :  rejectPaypalTransaction,
    payWithPaypal : payWithPaypal,
    checkpayWithPaypal : checkpayWithPaypal,
};