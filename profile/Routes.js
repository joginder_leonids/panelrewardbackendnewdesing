const   express = require('express'),
        mappingRouter = express.Router(),
        mid = require('../Middleware/middleware'),
        mappingController = require('./Controller');

// --------- Router --------

//Register Route
        
mappingRouter.route('/getProfileById/:id')
        .get(mappingController.getProfileById)            
       
mappingRouter.route('/updateProfile/:id')
        .post(mappingController.updateProfile)       


       
module.exports = mappingRouter;